
class Osoba:
    wiek = 0

    def __init__(self, imie, nazwisko):
        self.imie = imie
        self.nazwisko = nazwisko

    def age(self):
        self.wiek = wiek

    def czy_moge_kupic_alkohol(self):
        return self.wiek > 18

def dane_osobowe(czlowiek):
    print("")
    print(czlowiek.imie)
    print(czlowiek.nazwisko)
    print(czlowiek.wiek)
    print(czlowiek.czy_moge_kupic_alkohol())
    print("")

if __name__ == "__main__":
    zuza = Osoba("Zuza","Jot")
    zuza.wiek = 20
    adam = Osoba("Adam","Nowak")
    adam.wiek = 15

    print("Przedstawiam Zuzę i Adama:")
    dane_osobowe(zuza)
    print(f"Osoba ma na imie {zuza.imie}, a nazywa się {zuza.nazwisko}. Ma {zuza.wiek} lat, więc twierdzenie, że może kupić alkohol jest {zuza.czy_moge_kupic_alkohol()}.")
    dane_osobowe(adam)
    print(f"Osoba ma na imie {adam.imie}, a nazywa się {adam.nazwisko}. Ma {adam.wiek} lat, więc twierdzenie, że może kupić alkohol jest {adam.czy_moge_kupic_alkohol()}.")

    zuza.wiek = 9
    dane_osobowe(zuza)
    print(f"Osoba ma na imie {zuza.imie}, a nazywa się {zuza.nazwisko}. Ma {zuza.wiek} lat, więc twierdzenie, że może kupić alkohol jest {zuza.czy_moge_kupic_alkohol()}.")

